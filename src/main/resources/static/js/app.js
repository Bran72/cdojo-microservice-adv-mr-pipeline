$( document ).ready(function() {
    $("#server-info-btn").click (function () {
        $.get("/api/hello", function (data) {
            $("#server-info").text("HostName : "+data.hostName+" server time : "+data.currentDateTime);
            $("#server-info-panel").removeClass("hidden");
        });
    });
    $("#list-person-btn").click (function () {
        $.get("/api/person", function (data) {
            console.log(data);
            $("#list").empty();
            $("#list").append("<ul>");
            $.each(data, function (index, value) {
                $("#list").append("<li>"+value.firstName+" "+value.lastName+"</li>");
            })
            $("#list").append("</ul>");
            $("#list-panel").removeClass("hidden");
        });
    })
  });