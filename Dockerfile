FROM openjdk:8
EXPOSE 8080
ENV JAVA_OPTS -Xmx200m
RUN mkdir /deploy
ADD target/*.jar /deploy/app.jar
CMD java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar /deploy/app.jar
